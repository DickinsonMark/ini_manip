import os

USER = os.getenv('API_USER')

refName = os.environ.get("CI_COMMIT_REF_NAME")
piplineID = os.environ.get("CI_PIPELINE_ID")
notification = os.environ.get("Notifications_Adapter_SMS_Account_Token")
relVersion = refName + ".0." + piplineID

version = relVersion.replace("rel.", "")
print("current version is", version)
print(f"current notification={notification}")
print("export VERSION='{}'".format(version))
print("export notification='{}'".format(notification))