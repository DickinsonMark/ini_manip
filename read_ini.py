import configparser
import os

config = configparser.ConfigParser()
config.read('application.ini')
print(config['base']['notifications.adapter.sms.account.sid'])     # -> "/path/name/"
config['base']['notifications.adapter.sms.account.sid'] = '/var/shared/'    # update
config['base']['default_message'] = 'Hey! help me!!'   # create
print(f"current notification has the new value in application.ini {config['base']['notifications.adapter.sms.account.sid']}")
with open('application.ini', 'w') as configfile:    # save
    config.write(configfile)